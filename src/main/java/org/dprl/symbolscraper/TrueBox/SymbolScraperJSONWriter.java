package org.dprl.symbolscraper.TrueBox;

import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.*;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SymbolScraperJSONWriter {
        ArrayList<PageStructure> allPages;
        Config conf;
        PDDocumentInformation docInfo;
        String out_filename;

        public SymbolScraperJSONWriter(ArrayList<PageStructure> allPages,
                                      Config conf, PDDocumentInformation docInfo, String out_filename ){
            this.allPages=allPages;
            this.conf = conf;
            this.docInfo = docInfo;
            this.out_filename = out_filename;
        }

        public static float validateValue(float value){
            Float temp = value;
            if(temp.isNaN() || temp.isInfinite()){
                return 0.0f;
            }else{
                return value;
            }
        }

        public static JSONObject validateBBOX(BBOX boundingBox){
            JSONObject bbox = new JSONObject();
            bbox.put("x",validateValue(boundingBox.startX));
            bbox.put("y",validateValue(boundingBox.startY));
            bbox.put("width",validateValue(boundingBox.width));
            bbox.put("height",validateValue(boundingBox.height));
            return bbox;
        }

        public JSONObject getPageElement(PageStructure page){
            JSONObject pageJSON = page.asJSON(page.config);
            JSONArray linesJSON = new JSONArray();

            for (Line l: page.lines) {
                linesJSON.put(getLineElement(l));
            }

            JSONObject text = new JSONObject();
            text.put("textLines", linesJSON);
            pageJSON.put("graphics", page.jsonObject.get("ROOT"));
            pageJSON.put("text", text);

            return pageJSON;
        }

        public JSONObject getLineElement(Line line) {
            JSONObject lineElem = line.asJSON(conf);
            JSONArray wordsJSON = new JSONArray();
            for (Word w: line.getWords()) {
                wordsJSON.put(getWordElement(w));
            }
            lineElem.put("words",wordsJSON);
            return lineElem;
        }

        public JSONObject getWordElement(Word word) {

            JSONObject wordElem = word.asJSON(conf);
            JSONArray constituents = new JSONArray();
            for (PathWrapper wordEntry: word.getConstituents()) {
                constituents.put(wordEntry.asJSON(conf));
            }
            wordElem.put("constituents",constituents);
            return wordElem;
        }

        public void writeJSON(){
            JSONObject json = new JSONObject();
            ArrayList<JSONObject> pages = new ArrayList<>();

            for (PageStructure page: allPages) {
                page.setConfig(conf);
                pages.add(getPageElement(page));
            }

            json.put( "docTitle",( docInfo.getTitle() == null) ? "missing" : docInfo.getTitle());
            json.put( "docAuthor",( docInfo.getAuthor() == null) ? "missing" : docInfo.getAuthor());
            json.put("pages",pages);
            try {
                FileWriter file = new FileWriter(out_filename);
                file.write(json.toString());
                file.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public JSONObject getJSON(){
            JSONObject json = new JSONObject();
            ArrayList<JSONObject> pages = new ArrayList<>();

            for (PageStructure page: allPages) {
                page.setConfig(conf);
                pages.add(getPageElement(page));
            }

            json.put( "docTitle",( docInfo.getTitle() == null) ? "missing" : docInfo.getTitle());
            json.put( "docAuthor",( docInfo.getAuthor() == null) ? "missing" : docInfo.getAuthor());
            json.put("pages",pages);

            return json;
        }
}
