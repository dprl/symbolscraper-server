package org.dprl.symbolscraper.TrueBox.PostProcessor;
import org.dprl.symbolscraper.TrueBox.Domain.JTSgraphics.LabeledGeometryCollection;
import org.dprl.symbolscraper.TrueBox.Domain.JTSgraphics.LabeledLineString;
import org.dprl.symbolscraper.TrueBox.Domain.Line;
import org.dprl.symbolscraper.TrueBox.Domain.PageStructure;
import org.eclipse.jetty.util.ajax.JSON;
import org.json.JSONArray;
import org.locationtech.jts.algorithm.ConvexHull;
import org.locationtech.jts.algorithm.distance.PointPairDistance;
import org.locationtech.jts.geom.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Function;
import org.json.JSONObject;

import static org.locationtech.jts.algorithm.Angle.*;

public class ExplainGeometry implements Function<PageStructure, PageStructure> {


    public JSONArray getLineFromRectangle(JSONObject rectangle){
        double x1 = (  (JSONObject)  ((JSONArray)rectangle.get("points")).get(0) ).getDouble("x");
        double y1 = (  (JSONObject)  ((JSONArray)rectangle.get("points")).get(0) ).getDouble("y");
        double x2 = (  (JSONObject)  ((JSONArray)rectangle.get("points")).get(1) ).getDouble("x");
        double y2 = (  (JSONObject)  ((JSONArray)rectangle.get("points")).get(1) ).getDouble("y");

        JSONArray json = new JSONArray();
        JSONObject p1 = new JSONObject();
        JSONObject p2 = new JSONObject();
        if(Math.abs(x1-x2)>Math.abs(y1-y2)){
            //side
            Coordinate _p1 = LineSegment.midPoint(new Coordinate(x1,y1),new Coordinate(x1,y2));

            p1.put("x",x1);
            p1.put("y",_p1.y);
            Coordinate _p2 = LineSegment.midPoint(new Coordinate(x2,y1),new Coordinate(x2,y2));

            p2.put("x",x2);
            p2.put("y",_p2.y);

        }else{
            //top
            Coordinate _p1 = LineSegment.midPoint(new Coordinate(x1,y1),new Coordinate(x2,y1));
            p1.put("x",_p1.x);
            p1.put("y",y1);
            Coordinate _p2 = LineSegment.midPoint(new Coordinate(x1,y2),new Coordinate(x2,y2));
            p2.put("x",_p2.x);
            p2.put("y",y2);
        }
        json.put(p1);
        json.put(p2);
        return json;
    }

    public JSONArray getLineFromGeometry(Geometry line1, Geometry line2, LineString[] lineStrings, double lineWidth){





        Coordinate p1l1 = line1.getCoordinates()[0];
        Coordinate p2l1 = line1.getCoordinates()[1];
        Coordinate p1l2 = line2.getCoordinates()[0];
        Coordinate p2l2 = line2.getCoordinates()[1];

        Coordinate _p1;
        Coordinate _p2;

        if(p1l1.distance(p1l2)<p1l1.distance(p1l1)){
            _p1 = LineSegment.midPoint(p1l1, p1l2);
            _p2 = LineSegment.midPoint(p2l1, p2l2);
        }else{
            _p1 = LineSegment.midPoint(p1l1, p2l2);
            _p2 = LineSegment.midPoint(p2l1, p1l2);
        }


        LineSegment newLine = new LineSegment(_p1, _p2);
        double angle = newLine.angle();
        double extension = newLine.getLength();
        double extendX = extension*Math.cos(angle);
        double extendY = extension*Math.sin(angle);

        GeometryFactory GF = new GeometryFactory();

        if (_p1.x<_p2.x){
            extendX = Math.abs(extendX);
        }else{
            extendX = (-1)*Math.abs(extendX);
        }
        //pemding
        if (_p1.y<_p2.y){
            extendY = Math.abs(extendY);
        }else{
            extendY = (-1)*Math.abs(extendY);
        }

        Coordinate[] coordinateArrayNewLine = {
                new Coordinate(_p1.x-extendX, _p1.y-extendY),
                new Coordinate(_p2.x+extendX, _p2.y+extendY)
        };

        LineString lineStringNewLine = GF.createLineString(coordinateArrayNewLine);
        double newLength = lineStringNewLine.getLength();

        MultiLineString polygon = new MultiLineString(lineStrings, GF);
        Geometry A = polygon.intersection(lineStringNewLine);
        int intersections = A.getCoordinates().length;
        JSONArray json = new JSONArray();
        JSONObject p1 = new JSONObject();
        JSONObject p2 = new JSONObject();
        if (intersections > 1){
            Point p1G = (Point) A.getGeometryN(0);
            Point p2G = (Point) A.getGeometryN(1);



            /*p1.put("x",_p1.x);
            p1.put("y",_p1.y);
            p2.put("x",_p2.x);
            p2.put("y",_p2.y);*/
            if (p1G.getX()>p2G.getX()){
                p1.put("x",p1G.getX()+lineWidth/2);
                p2.put("x",p2G.getX()-lineWidth/2);
            }else{
                p1.put("x",p1G.getX()-lineWidth/2);
                p2.put("x",p2G.getX()+lineWidth/2);
            }
            if (p1G.getY()>p2G.getY()){
                p1.put("y",p1G.getY()+lineWidth/2);
                p2.put("y",p2G.getY()-lineWidth/2);
            }else{
                p1.put("y",p1G.getY()-lineWidth/2);
                p2.put("y",p2G.getY()+lineWidth/2);
            }
        }else{
            p1.put("x",_p1.x);
            p1.put("y",_p1.y);
            p2.put("x",_p2.x);
            p2.put("y",_p2.y);
        }
        json.put(p1);
        json.put(p2);
        return json;

    }

    public JSONArray getAproximatedGeometry(JSONArray geometryCollection, ArrayList<Geometry> geometries_temp){
        if(geometryCollection.length()==1 && ((JSONObject)geometryCollection.get(0)).get("typeFromPDF") == "rectangle"){
            return getLineFromRectangle( (JSONObject)geometryCollection.get(0) );
        }else{
            int counter = 0;
            double maxLength = 0;
            double secondMaxLength = 0;
            int maxLengthIndex = 0;
            int secondMaxLengthIndex = 0;
            double perimeter = 0;

            double lineWidth = ((JSONObject) geometryCollection.get(0)).getDouble("lineWidth");

            LineString[] lineStrings = new LineString[geometries_temp.size()];

            for (Geometry item: geometries_temp) {
                lineStrings[counter] = (LabeledLineString)item;
                double currentLength = item.getLength();
                perimeter += currentLength;
                if(currentLength > maxLength){

                    secondMaxLength = maxLength;
                    secondMaxLengthIndex = maxLengthIndex;

                    maxLength = currentLength;
                    maxLengthIndex = counter;

                } else if (currentLength > secondMaxLength) {
                    secondMaxLength = currentLength;
                    secondMaxLengthIndex = counter;
                }
                counter+=1;
            }
            LineString maxLine = (LineString)geometries_temp.get(maxLengthIndex);
            LineString secondMaxLine = (LineString)geometries_temp.get(secondMaxLengthIndex);

            double angleDiff = Math.abs(getAngleLine(maxLine)-getAngleLine(secondMaxLine));
            double ratio = (secondMaxLength+maxLength)/perimeter;
            if(ratio > 0.85 && angleDiff < 5){
                //ConvexHull convexHull = new ConvexHull(new MultiLineString(lineStrings, new GeometryFactory()));
                //Geometry convexHullGeometry = convexHull.getConvexHull();
                return getLineFromGeometry(geometries_temp.get(maxLengthIndex),geometries_temp.get(secondMaxLengthIndex), lineStrings, lineWidth);
            }
        }
        return null;
    }

    public double getAngleLine(LineString line){
        //TODO: dirty fix later
        double angleDiff = 0;
        if (line.getCoordinateN(0).x == line.getCoordinateN(1).x){
            if (line.getCoordinateN(0).y < line.getCoordinateN(1).y){
                angleDiff = angle(line.getCoordinateN(0), line.getCoordinateN(1));
            }else{
                angleDiff = angle(line.getCoordinateN(1), line.getCoordinateN(0));
            }
        }else if(line.getCoordinateN(0).x < line.getCoordinateN(1).x){
            angleDiff = angle(line.getCoordinateN(0), line.getCoordinateN(1));
        }else{
            angleDiff = angle(line.getCoordinateN(1), line.getCoordinateN(0));
        }
        return toDegrees(angleDiff);
    }

    public JSONObject getAproxLineAttributes(JSONArray points){
        double x1 = (  (JSONObject)  points.get(0) ).getDouble("x");
        double y1 = (  (JSONObject)  points.get(0) ).getDouble("y");
        double x2 = (  (JSONObject)  points.get(1) ).getDouble("x");
        double y2 = (  (JSONObject)  points.get(1) ).getDouble("y");

        Coordinate p1 = new CoordinateXY(x1,y1);
        Coordinate p2 = new CoordinateXY(x2,y2);

        LineSegment line = new LineSegment(p1, p2);

        Double angle = toDegrees(normalizePositive(angle(p1, p2)));
        Double length = line.getLength();

        JSONObject attr = new JSONObject();
        attr.put("angle",angle);
        attr.put("length",length);
        return attr;
    }

    public JSONObject handleLine(JSONObject currentSubGeometry, LabeledLineString geometry){
        Coordinate[] coordinates = geometry.getCoordinates();
        double radians = angle(coordinates[0], coordinates[1]);
        Double angle = toDegrees(normalizePositive(radians));
        JSONArray points = new JSONArray();

        double lineWidth = currentSubGeometry.getDouble("lineWidth");
        double marginLength = lineWidth/2;

        //double xMargin = Math.abs(marginLength*Math.cos(radians));
        //double yMargin = Math.abs(marginLength*Math.sin(radians));

        double xMargin = 0;
        double yMargin = 0;

        double p1x = coordinates[0].x + (coordinates[0].x>coordinates[1].x?xMargin:-xMargin);
        double p1y = coordinates[0].y + (coordinates[0].y>coordinates[1].y?yMargin:-yMargin);
        double p2x = coordinates[1].x + (coordinates[0].x>coordinates[1].x?-xMargin:xMargin);
        double p2y = coordinates[1].y + (coordinates[0].y>coordinates[1].y?-yMargin:yMargin);

        JSONObject point1 = new JSONObject();
        point1.put("x", p1x);
        point1.put("y", p1y);
        JSONObject point2 = new JSONObject();
        point2.put("x", p2x);
        point2.put("y", p2y);

        points.put(point1);
        points.put(point2);

        currentSubGeometry.put("points",points);
        currentSubGeometry.put("angle",angle);
        currentSubGeometry.put("length",geometry.getLength());

        return currentSubGeometry;
    }
    public JSONObject handleRectangle(JSONObject currentSubGeometry, LabeledLineString geometry){
        Coordinate[] coordinates = geometry.getCoordinates();
        JSONArray points = new JSONArray();

        JSONObject bottomLeft = new JSONObject();
        bottomLeft.put("x", Math.min(coordinates[0].x, Math.min(
                coordinates[1].x,
                Math.min(
                        coordinates[2].x,
                        coordinates[3].x
                ))));
        bottomLeft.put("y", Math.min(coordinates[0].y, Math.min(
                coordinates[1].y,
                Math.min(
                        coordinates[2].y,
                        coordinates[3].y
                ))));

        JSONObject topRight = new JSONObject();
        topRight.put("x", Math.max(coordinates[0].x, Math.max(
                coordinates[1].x,
                Math.max(
                        coordinates[2].x,
                        coordinates[3].x
                ))));
        topRight.put("y", Math.max(coordinates[0].y, Math.max(
                coordinates[1].y,
                Math.max(
                        coordinates[2].y,
                        coordinates[3].y
                ))));
        points.put(bottomLeft);
        points.put(topRight);
        currentSubGeometry.put("points",points);

        return currentSubGeometry;
    }

    public JSONObject handleCurve(JSONObject currentSubGeometry, LabeledLineString geometry){
        Coordinate[] coordinates = geometry.getCoordinates();
        JSONArray points = new JSONArray();
        JSONArray sizes = new JSONArray();
        for (int i = 0; i < coordinates.length; i++) {
            JSONObject point = new JSONObject();
            point.put("x",coordinates[i].x);
            point.put("y",coordinates[i].y);
            points.put(point);
            if(i != coordinates.length-1) {
                PointPairDistance temp = new PointPairDistance();
                temp.initialize(coordinates[i], coordinates[i + 1]);
                sizes.put(temp.getDistance());
            }
        }

        JSONObject p1 = new JSONObject();
        p1.put("x",Double.parseDouble(geometry.getAttributes().get("x1")));
        p1.put("y",Double.parseDouble(geometry.getAttributes().get("y1")));
        JSONObject p2 = new JSONObject();
        p2.put("x",Double.parseDouble(geometry.getAttributes().get("x2")));
        p2.put("y",Double.parseDouble(geometry.getAttributes().get("y2")));
        JSONObject p3 = new JSONObject();
        p3.put("x", Double.parseDouble(geometry.getAttributes().get("x3")));
        p3.put("y", Double.parseDouble(geometry.getAttributes().get("y3")));
        JSONObject bezierPoints = new JSONObject();
        bezierPoints.put("p1",p1);
        bezierPoints.put("p2",p2);
        bezierPoints.put("p3",p3);
        currentSubGeometry.put("bezierPoints",bezierPoints);
        currentSubGeometry.put("points",points);
        currentSubGeometry.put("curvedTo",geometry.getAttributes().get("curvedTo"));
        currentSubGeometry.put("lengthSegments",sizes);

        return currentSubGeometry;
    }


    @Override
    public PageStructure apply(PageStructure pageStructure) {
        JSONObject json = new JSONObject();
        JSONArray objectsCollections = new JSONArray();

        JSONObject BBOXpage = new JSONObject();
        BBOXpage.put("x",pageStructure.meta.pdfBoundingBox.startX);
        BBOXpage.put("y",pageStructure.meta.pdfBoundingBox.startY);
        BBOXpage.put("width",pageStructure.meta.pdfBoundingBox.width);
        BBOXpage.put("height",pageStructure.meta.pdfBoundingBox.height);
        json.put("BBOXpage", BBOXpage);

        int id = -1;
        for (LabeledGeometryCollection collection: pageStructure.graphicsJTS) {
            id+=1;

            if (id == 29){
                int a  = 2;
            }

            JSONObject currentGeometry = new JSONObject();
            JSONObject BBOX = new JSONObject();
            BBOX.put("x",collection.getBoundingBox().startX);
            BBOX.put("y",collection.getBoundingBox().startY);
            BBOX.put("width",collection.getBoundingBox().width);
            BBOX.put("height",collection.getBoundingBox().height);
            currentGeometry.put("BBOX", BBOX);
            currentGeometry.put("graphicCollectionID", id);

            boolean isRectangle = false;

            boolean filled =  collection.getAttributes().get("filled") != null;
            if(filled){
                currentGeometry.put("filled",collection.getAttributes().get("filled"));
            }

            GeometryCollectionIterator iterator = new GeometryCollectionIterator(collection);
            JSONArray objects = new JSONArray();
            iterator.next();
            int inner_id = -1;
            boolean isLine = false;
            ArrayList<Geometry> geometries_temp = new ArrayList();
            while (iterator.hasNext()) {
                inner_id+=1;
                JSONObject currentSubGeometry = new JSONObject();
                LabeledLineString geometry = (LabeledLineString) iterator.next();
                geometries_temp.add(geometry);
                currentSubGeometry.put("typeFromPDF",geometry.getAttributes().get("typeFromPDF"));
                currentSubGeometry.put("lineWidth",Double.parseDouble(geometry.getAttributes().get("lineWidth")));

                if(Objects.equals(geometry.getAttributes().get("typeFromPDF"), "line")){
                    isLine = true;
                    currentSubGeometry = handleLine(currentSubGeometry, geometry);
                }
                if(Objects.equals(geometry.getAttributes().get("typeFromPDF"), "rectangle")){
                    isRectangle=true;
                    currentSubGeometry = handleRectangle(currentSubGeometry, geometry);
                }
                if(Objects.equals(geometry.getAttributes().get("typeFromPDF"), "curve")){
                    currentSubGeometry = handleCurve(currentSubGeometry, geometry);
                }
                objects.put(currentSubGeometry);
                currentSubGeometry.put("graphicObjectID", inner_id);
            }
            currentGeometry.put("GeometryCollection",objects);
            currentGeometry.put("fillingColor", collection.getAttributes().get("fillingColor"));
            currentGeometry.put("strokingColor", collection.getAttributes().get("strokingColor"));
            String fillingColor = collection.getAttributes().get("fillingColor");
            currentGeometry.put("isLine", false);
            if(filled && !fillingColor.equals("#FFFFFF") && (geometries_temp.size()>1 || isRectangle)){
                JSONArray temp = getAproximatedGeometry(objects,geometries_temp);
                if(temp != null) {
                    JSONObject attributes = getAproxLineAttributes(temp);
                    attributes.put("points", temp);
                    currentGeometry.put("approxLine", attributes);
                    currentGeometry.put("isLine", true);
                }
            }
            if(isLine && geometries_temp.size()==1)
                currentGeometry.put("isLine", true);
            objectsCollections.put(currentGeometry);
        }

        json.put("ROOT",new JSONArray(objectsCollections));
        pageStructure.jsonObject = json;

        return pageStructure;
    }

}


