package org.dprl.symbolscraper.TrueBox.PostProcessor;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.encoding.GlyphList;
import org.dprl.symbolscraper.TrueBox.Domain.*;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Function;


import org.dprl.symbolscraper.TrueBox.Domain.*;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.util.ArrayList;
import java.util.function.Function;

public class CharIdentificationPostProcessor implements Function<PageStructure, PageStructure> {

    public GeneralPath normalizePath(GeneralPath path, int scale){
        Rectangle r = path.getBounds();
        AffineTransform transformDisplacement = new AffineTransform();
        AffineTransform transformScaling = new AffineTransform();
        transformDisplacement.setTransform(1,0,0,1,-path.getBounds().x,-path.getBounds().y);
        double sx = scale/((double)path.getBounds().width);
        double sy = scale/((double)path.getBounds().height);
        transformScaling.setToScale(sx,sy);
        path.transform(transformDisplacement);
        path.transform(transformScaling);
        return path;
    }

    public int pathSimilarity(GeneralPath path1, GeneralPath path2, int scale){
        int score = 0;
        for (int i = 0; i < scale; i++) {
            for (int j = 0; j < scale; j++) {
                Point point = new Point(i,j);
                if(path1.contains(point) && path2.contains(point)){
                    score+=1;
                }

            }
        }
        return score;
    }
    @Override
    public PageStructure apply(PageStructure pageStructure) {
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage( page );

        PDType1Font font = PDType1Font.COURIER;
        for (Graphic item: pageStructure.graphics) {
            if(item.getBoundingBox().width+item.getBoundingBox().height>20){
                continue;
            }
            int scale = 100;
            GeneralPath path_graphic = item.getGraphicConstituents().get(0).getGeneralPath();
            path_graphic = normalizePath(path_graphic,scale);
            GeneralPath char_path = null;
            char[] alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-,".toCharArray();
            String best = "";
            int bestScore = 0;
            for (Character c: alphabet) {
                try {
                    char_path = font.getPath(c.toString());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                char_path = normalizePath(char_path,scale);
                int score = pathSimilarity(path_graphic,char_path,scale);
                if(score>bestScore){
                    bestScore=score;
                    best = c.toString();
                }
            }
            item.mostSimiliarChar = best;
        }
        return pageStructure;
    }


}


