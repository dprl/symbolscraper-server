package org.dprl.symbolscraper.TrueBox.PostProcessor;

import org.dprl.symbolscraper.TrueBox.Domain.*;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.util.ArrayList;
import java.util.function.Function;

public class SplicePostProcessor implements Function<PageStructure, PageStructure> {

    @Override
    public PageStructure apply(PageStructure pageStructure) {
        return spliceGraphicsItemsIntoWords(pageStructure);
    }

    public PageStructure spliceGraphicsItemsIntoWords(PageStructure pageStructure) {

        ArrayList<Graphic> unclaimedGraphics = new ArrayList<>();

        for (Graphic g: pageStructure.graphics) {
            Line l = lineGraphicBelongsToo(pageStructure.lines, g);
            if (l == null) {
                unclaimedGraphics.add(g);
            } else {
                spliceIntoLine(g, l);
                l.update();
            }
        }

        pageStructure.graphics = unclaimedGraphics;
        return pageStructure;
    }

    private Line lineGraphicBelongsToo(ArrayList<Line> lines, Graphic g) {
        Line highestIOULine = null;
        float highestIOU = 0;
        for(Line l: lines) {
            if (g.overlap(l)) {
                float curIOU = l.intersectionOverUnion(g);
                if (curIOU > highestIOU) {
                    highestIOU = curIOU;
                    highestIOULine = l;
                }
            }
        }
        return highestIOULine;
    }

    private void spliceIntoLine(Graphic g, Line l) {
        boolean belongToAWord = false;
        for(Word w: l.getWords()) {
            if (w.overlap(g)) {
                belongToAWord = true;
                spliceIntoWord(g, w);
                w.update();
            }
        }
        if (!belongToAWord) {
            l.consume(g, true);
        }
    }

    private void spliceIntoWord(Graphic g, Word w) {
        boolean belongToAChar = false;
        for(PathWrapper c: w.getConstituents()) {
            if ((c instanceof Char) && graphicsItemBelongsToChar(g, (Char) c)) {
                belongToAChar = true;
                c.consume(g, true);
            }
        }
        if (!belongToAChar) {
            w.consume(g, true);
        }
    }

    private boolean graphicsItemBelongsToChar(Graphic g, Char c) {
        boolean overlaps = c.overlap(g);
        boolean intersects = c.intersect(g);
        return overlaps && intersects;
    }
}
