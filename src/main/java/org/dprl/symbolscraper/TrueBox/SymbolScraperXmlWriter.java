package org.dprl.symbolscraper.TrueBox;

import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.*;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;


public class SymbolScraperXmlWriter {

    ArrayList<PageStructure> allPages;
    Config conf;
	PDDocumentInformation docInfo;

    public SymbolScraperXmlWriter(ArrayList<PageStructure> allPages, 
			Config conf, PDDocumentInformation docInfo ){
        this.allPages=allPages;
        this.conf = conf;
		this.docInfo = docInfo;
    }

    public Element getPageElement(PageStructure page){

        Element pageElem = page.asXml(conf);
		
        for (Line l: page.lines) {
            pageElem.add(getLineElement(l));
        }
        if (page.graphics.size() > 0) pageElem.add(unclaimedGraphicsItems(page.graphics));

        return pageElem;
    }

    public Element unclaimedGraphicsItems(ArrayList<Graphic> graphicsItems) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("GraphicsItems");
        for (Graphic gi: graphicsItems) {
            root.add(gi.asXml(conf));
        }
        return root;
    }

    public Element getLineElement(Line line) {

        Element lineElem = line.asXml(conf);

        for (Word w: line.getWords()) {
            lineElem.add(getWordElement(w));
        }

        return lineElem;
    }

    public Element getWordElement(Word word) {

        Element wordElem = word.asXml(conf);

        for (PathWrapper wordEntry: word.getConstituents()) {
            wordElem.add(wordEntry.asXml(conf));
        }

        return wordElem;
    }

    public String writePDF() throws IOException {
        Document document = DocumentHelper.createDocument();
		Element pagesElem = document.addElement("Pages");
		
		// RZ: Adding title and author information in XML
		//-------------------------------------------------- 
		Element titleElem = pagesElem.addElement("Title");
		titleElem.addText( ( docInfo.getTitle() == null) ?
				"missing" : docInfo.getTitle());
		
		Element authorElem = pagesElem.addElement("Author");
		authorElem.addText( ( docInfo.getAuthor() == null)? 
				"missing" : docInfo.getAuthor() );
        

		// RZ: Disabling, leaving for later reference.
/*
        BBOX pdfBoundingBox = allPages.get(0).meta.pdfBoundingBox;
        if (conf.fields.writeFields.writePageBorderBBOX)
            pagesElem.addAttribute("BBOX", pdfBoundingBox.startX+ " " +
                                                + pdfBoundingBox.startY + " " +
                                                + pdfBoundingBox.width + " " +
                                                + pdfBoundingBox.height);
*/

        for (PageStructure page: allPages) {
            pagesElem.add(getPageElement(page));
        }
        StringWriter writer = new StringWriter();
        XMLWriter xmlWriter = new XMLWriter(writer, OutputFormat.createPrettyPrint());
        xmlWriter.write(document);
        xmlWriter.close();
        return writer.toString();
    }


}

