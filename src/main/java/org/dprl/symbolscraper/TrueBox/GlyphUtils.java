package org.dprl.symbolscraper.TrueBox;

import org.apache.fontbox.ttf.TrueTypeFont;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.text.TextPosition;
import org.apache.pdfbox.util.Matrix;
import org.dprl.symbolscraper.TrueBox.Domain.BBOX;
import java.lang.Math;

import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.io.IOException;

public class GlyphUtils {

    public static BBOX getBoundingBox(GeneralPath glyphPath) {
        PathIterator iterator = glyphPath.getPathIterator(null);

        double[] coordinates = new double[6];

        double maxX = Double.MIN_VALUE;
        double maxY = Double.MIN_VALUE;

        double minX = Double.MAX_VALUE;
        double minY = Double.MAX_VALUE;

        while(!iterator.isDone()) {
            int s = iterator.currentSegment(coordinates);

            switch (s) {
                case PathIterator.SEG_CLOSE:

                    break;

                case PathIterator.SEG_CUBICTO:

                    maxX = Math.max(maxX, coordinates[0]);
                    minX = Math.min(minX, coordinates[0]);

                    maxY = Math.max(maxY, coordinates[1]);
                    minY = Math.min(minY, coordinates[1]);

                    maxX = Math.max(maxX, coordinates[2]);
                    minX = Math.min(minX, coordinates[2]);

                    maxY = Math.max(maxY, coordinates[3]);
                    minY = Math.min(minY, coordinates[3]);

                    maxX = Math.max(maxX, coordinates[4]);
                    minX = Math.min(minX, coordinates[4]);

                    maxY = Math.max(maxY, coordinates[5]);
                    minY = Math.min(minY, coordinates[5]);
                    break;

                case PathIterator.SEG_LINETO:

                    maxX = Math.max(maxX, coordinates[0]);
                    minX = Math.min(minX, coordinates[0]);

                    maxY = Math.max(maxY, coordinates[1]);
                    minY = Math.min(minY, coordinates[1]);
                    break;

                case PathIterator.SEG_MOVETO:

                    maxX = Math.max(maxX, coordinates[0]);
                    minX = Math.min(minX, coordinates[0]);

                    maxY = Math.max(maxY, coordinates[1]);
                    minY = Math.min(minY, coordinates[1]);
                    break;

                case PathIterator.SEG_QUADTO:
                    maxX = Math.max(maxX, coordinates[0]);
                    minX = Math.min(minX, coordinates[0]);

                    maxY = Math.max(maxY, coordinates[1]);
                    minY = Math.min(minY, coordinates[1]);

                    maxX = Math.max(maxX, coordinates[2]);
                    minX = Math.min(minX, coordinates[2]);

                    maxY = Math.max(maxY, coordinates[3]);
                    minY = Math.min(minY, coordinates[3]);
                    break;
            }
            iterator.next();
        }

        float startX = (float) minX;
        float startY = (float) minY;

        float width =(float) (maxX - minX);
        float height = (float) (maxY-minY);

        return new BBOX(startX, startY, width, height);
    }

    public static int getEmSquare(TextPosition charInfo) {
        try{
            int emSquare = 0;
            int height = (int) (charInfo.getFont().getFontDescriptor().getCapHeight());
            if (height < 1024) {
                emSquare = 1000;
            } else {
                for (int i = 0; i < 6; i++) {
                    int power = ((int) Math.pow(2,i))*1024;
                    if(height<=power){
                        emSquare=power;
                        break;
                    }
                }
            }
            return emSquare;
        }catch (Exception e){
            return 1000;
        }
    }

    public static GeneralPath translatePath(GeneralPath glyphPath, Matrix scalingMatrix, int emSquare) {

        float startX = scalingMatrix.getTranslateX();
        float startY = scalingMatrix.getTranslateY();

        AffineTransform at = new AffineTransform();
        at.translate(startX, startY);
        at.scale(scalingMatrix.getScaleX()/emSquare, scalingMatrix.getScaleY()/emSquare);

        GeneralPath newGlyph = (GeneralPath) glyphPath.clone();
        newGlyph.transform(at);
        return newGlyph;

    }
}

