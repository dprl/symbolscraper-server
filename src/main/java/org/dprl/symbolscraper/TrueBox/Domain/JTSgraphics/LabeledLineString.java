package org.dprl.symbolscraper.TrueBox.Domain.JTSgraphics;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;

public class LabeledLineString extends LineString {
    private Dictionary<String, String> attributes = new Hashtable<>();
    public LabeledLineString(CoordinateSequence points, GeometryFactory factory) {
        super(points, factory);
    }
    public void addAttribute(String name, String value){
        attributes.put(name, value);
    }

    public Dictionary<String, String> getAttributes() {
        return attributes;
    }

    public static LabeledLineString createFromPoints(ArrayList<Point2D> points, GeometryFactory factory){
        Coordinate[] coordinates = new Coordinate[points.size()];
        for (int i = 0; i < points.size(); i++) {
            Point2D point = points.get(i);
            coordinates[i] = new Coordinate((float) point.getX(), (float) point.getY());
        }
        return new LabeledLineString(new CoordinateArraySequence(coordinates), factory);
    }
}
