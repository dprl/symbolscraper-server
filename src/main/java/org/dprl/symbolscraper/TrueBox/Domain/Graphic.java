package org.dprl.symbolscraper.TrueBox.Domain;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.Boundable;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class Graphic extends PathWrapper {

    public String descriptor;
    public int graphicId;

    public String mostSimiliarChar;

    private Graphic(ArrayList<GraphicGlyph> gGlyphs, String descriptor, int graphicId) {
        this.descriptor = descriptor;
        this.charConstituents = new ArrayList<>();
        this.graphicConstituents = gGlyphs;
        this.graphicId = graphicId;
        this.mostSimiliarChar = "";
    }

    public Graphic(GraphicGlyph initGraphicGlyph, String descriptor, int graphicId) {
        graphicConstituents = new ArrayList<>();
        graphicConstituents.add(initGraphicGlyph);
        charConstituents = new ArrayList<>();

        updateBoundingBox(initGraphicGlyph);
        this.descriptor = descriptor;
        this.graphicId = graphicId;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            Graphic g = new Graphic(graphicConstituents, descriptor, graphicId);
            g.setBoundingBox(boundingBox);
            g.charConstituents = charConstituents;
            return g;
        }
    }

    public Element asXml(Config c) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("Graphic");
        if (graphicConstituents.size() == 1) {
            GraphicGlyph singleGraphicsItem = graphicConstituents.get(0);
            root.addText(singleGraphicsItem.descriptor);
            if(c.fields.writeFields.writePoints) {
                StringBuilder ptsStr = new StringBuilder();
                for (Point2D pt: singleGraphicsItem.points) {
                    ptsStr.append("p ").append(pt.getX()).append(" ").append(pt.getY()).append(" ");
                }
                root.addAttribute("points", ptsStr.toString());
            }
            if (c.fields.writeFields.writePath){
                Element pathNode = root.addElement("Path");
                int elements = 0;
                GeneralPath path = singleGraphicsItem.getGeneralPath();
                double[] coordinates = new double[6];
                String points;
                String currentPoint = "";
                String firstPoint = "";
                Element line;
                Element bezierQuad;
                Element bezierCubic;
                for (PathIterator pi = path.getPathIterator(null); !pi.isDone(); pi.next()) {
                    elements+=1;
                    int type = pi.currentSegment(coordinates);
                    switch (type) {
                        case PathIterator.SEG_MOVETO:
                            elements-=1;
                            currentPoint = coordinates[0] + " " + coordinates[1];
                            firstPoint = currentPoint;
                            break;
                        case PathIterator.SEG_LINETO:
                            points = currentPoint + " "+coordinates[0] + " " + coordinates[1];
                            currentPoint = coordinates[0] + " " + coordinates[1];
                            line = pathNode.addElement("Line");
                            line.addAttribute("points",points);
                            break;
                        case PathIterator.SEG_QUADTO:
                            points = currentPoint + " "+coordinates[0] + " " + coordinates[1]+" "+coordinates[2] + " " + coordinates[3];
                            currentPoint = coordinates[2] + " " + coordinates[3];
                            bezierQuad = pathNode.addElement("BezierQuad");
                            bezierQuad.addAttribute("points",points);
                            break;
                        case PathIterator.SEG_CUBICTO:
                            points = currentPoint + " "+coordinates[0] + " " + coordinates[1]+" "+coordinates[2] + " " + coordinates[3]+" "+coordinates[4] + " " + coordinates[5];
                            currentPoint = coordinates[4] + " " + coordinates[5];
                            bezierCubic = pathNode.addElement("bezierCubic");
                            bezierCubic.addAttribute("points",points);
                            break;
                        case PathIterator.SEG_CLOSE:
                            points = currentPoint + " "+firstPoint;
                            line = pathNode.addElement("Line");
                            line.addAttribute("points",points);
                            currentPoint = "";
                            break;
                        default:
                            break;
                    }
                }
                pathNode.addAttribute("items",elements+"");
            }
        } else if (graphicConstituents.size() > 1) {
            if (c.fields.writeFields.writeId) root.addAttribute("id", this.graphicId + "");
            root.addAttribute("descriptor", descriptor);
            for (GraphicGlyph gi : graphicConstituents) {
                root.add(gi.asXml(c));
            }
        }
        if (c.fields.writeFields.writeGraphicBBOX)
            root.addAttribute("BBOX", this.boundingBox.startX + " " +
                    +this.boundingBox.startY + " " +
                    +this.boundingBox.width + " " +
                    +this.boundingBox.height);

        if (c.fields.writeFields.writeId)
            root.addAttribute("id", this.graphicId+"");

        root.addAttribute("similarCharacter",this.mostSimiliarChar);

        return root;
    }

    public JSONObject asJSON(Config c) {
        JSONObject root = new JSONObject();
        if (graphicConstituents.size() == 1) {
            GraphicGlyph singleGraphicsItem = graphicConstituents.get(0);
            root.put("descriptor",singleGraphicsItem.descriptor);
            if(c.fields.writeFields.writePoints) {
                StringBuilder ptsStr = new StringBuilder();
                for (Point2D pt: singleGraphicsItem.points) {
                    ptsStr.append("p ").append(pt.getX()).append(" ").append(pt.getY()).append(" ");
                }
                root.put("points", ptsStr.toString());
            }
            /*
            if (c.fields.writeFields.writePath){
                Element pathNode = root.addElement("Path");
                JSONObject pathJSON = new JSONObject();
                int elements = 0;
                GeneralPath path = singleGraphicsItem.getGeneralPath();
                double[] coordinates = new double[6];
                String points;
                String currentPoint = "";
                String firstPoint = "";
                Element line;
                Element bezierQuad;
                Element bezierCubic;
                for (PathIterator pi = path.getPathIterator(null); !pi.isDone(); pi.next()) {
                    elements+=1;
                    int type = pi.currentSegment(coordinates);
                    switch (type) {
                        case PathIterator.SEG_MOVETO:
                            elements-=1;
                            currentPoint = coordinates[0] + " " + coordinates[1];
                            firstPoint = currentPoint;
                            break;
                        case PathIterator.SEG_LINETO:
                            points = currentPoint + " "+coordinates[0] + " " + coordinates[1];
                            currentPoint = coordinates[0] + " " + coordinates[1];
                            line = pathNode.addElement("Line");
                            line.addAttribute("points",points);
                            break;
                        case PathIterator.SEG_QUADTO:
                            points = currentPoint + " "+coordinates[0] + " " + coordinates[1]+" "+coordinates[2] + " " + coordinates[3];
                            currentPoint = coordinates[2] + " " + coordinates[3];
                            bezierQuad = pathNode.addElement("BezierQuad");
                            bezierQuad.addAttribute("points",points);
                            break;
                        case PathIterator.SEG_CUBICTO:
                            points = currentPoint + " "+coordinates[0] + " " + coordinates[1]+" "+coordinates[2] + " " + coordinates[3]+" "+coordinates[4] + " " + coordinates[5];
                            currentPoint = coordinates[4] + " " + coordinates[5];
                            bezierCubic = pathNode.addElement("bezierCubic");
                            bezierCubic.addAttribute("points",points);
                            break;
                        case PathIterator.SEG_CLOSE:
                            points = currentPoint + " "+firstPoint;
                            line = pathNode.addElement("Line");
                            line.addAttribute("points",points);
                            currentPoint = "";
                            break;
                        default:
                            break;
                    }
                }
                pathNode.addAttribute("items",elements+"");
            }
             */
        } else if (graphicConstituents.size() > 1) {
            if (c.fields.writeFields.writeId) root.put("id", this.graphicId + "");
            root.put("descriptor", descriptor);
            JSONArray constituents = new JSONArray();
            for (GraphicGlyph gi : graphicConstituents) {
                constituents.put(gi.asXml(c));
            }
            root.put("constituents",constituents);
        }
        if (c.fields.writeFields.writeGraphicBBOX) {
            JSONObject bbox = new JSONObject();
            bbox.put("x", this.boundingBox.startX);
            bbox.put("y", this.boundingBox.startY);
            bbox.put("width", this.boundingBox.width);
            bbox.put("height", this.boundingBox.height);
            root.put("BBOX", bbox);
        }

        if (c.fields.writeFields.writeId)
            root.put("graphicID", this.graphicId);

        root.put("similarCharacter",this.mostSimiliarChar);

        return root;
    }

    @Override
    public void consume(Boundable boundable, boolean update) {
        if (boundable instanceof Char) {
            Char c = (Char) boundable;
            this.descriptor = this.descriptor + c.value;
            charConstituents.addAll(c.getCharConstituents());
            graphicConstituents.addAll(c.getGraphicConstituents());
        } else if (boundable instanceof CharGlyph) {
            CharGlyph cg = (CharGlyph) boundable;
            this.descriptor = this.descriptor + cg.value;
            charConstituents.add(cg);
        } else if (boundable instanceof GraphicGlyph) {
            GraphicGlyph gg = (GraphicGlyph) boundable;
            this.descriptor = this.descriptor + gg.descriptor;
            graphicConstituents.add(gg);
        } else if (boundable instanceof Graphic) {
            Graphic g = (Graphic) boundable;
            this.descriptor = this.descriptor + g.descriptor;
            charConstituents.addAll(g.getCharConstituents());
            graphicConstituents.addAll(g.getGraphicConstituents());
        }
        if (update) update();
    }
}
