package org.dprl.symbolscraper.TrueBox.Domain.Interfaces;

import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;

public abstract class GeneralPathType extends Boundable implements IGeneralPathType<GeneralPathType> {

    protected GeneralPath generalPath;

    public boolean intersect(GeneralPathType pathType) {


        Area thisArea = new Area(generalPath);
        Area otherArea = new Area(pathType.generalPath);

        // the intersection of the areas should work but it misses sometimes
        // it may have something to do with the "winding rule"
        PathIterator it = pathType.generalPath.getPathIterator(null);
        double[] coordinates = new double[6];
        while(!it.isDone()) {
            int s = it.currentSegment(coordinates);
            switch (s) {
                case PathIterator.SEG_CLOSE:
                    break;

                case PathIterator.SEG_CUBICTO:
                    if (thisArea.contains(coordinates[0], coordinates[1])) return true;
                    if (thisArea.contains(coordinates[2], coordinates[3])) return true;
                    if (thisArea.contains(coordinates[4], coordinates[5])) return true;
                    break;

                case PathIterator.SEG_LINETO:
                    if (thisArea.contains(coordinates[0], coordinates[1])) return true;
                    break;

                case PathIterator.SEG_MOVETO:
                    if (thisArea.contains(coordinates[0], coordinates[1])) return true;
                    break;

                case PathIterator.SEG_QUADTO:
                    if (thisArea.contains(coordinates[0], coordinates[1])) return true;
                    if (thisArea.contains(coordinates[2], coordinates[3])) return true;
                    break;
            }
            it.next();
        }
        otherArea.intersect(thisArea);
        return !thisArea.isEmpty();
    }

    @Override
    public void setPath(GeneralPath path) {
        this.generalPath = path;
    }

    public void update() {
        updateBoundingBox(this);
    }

    public void consume(Boundable intersectingWordEntry, boolean update) {
        // Don't need to consume at this level as we just keep a list of all glyphs in the wrapper classes
        // intentional no-op
    }
}
