package org.dprl.symbolscraper.TrueBox.Domain.Interfaces;

import org.dom4j.Element;
import org.dprl.config.Config;
import org.json.JSONObject;

public interface IXMLWriteable {
    Element asXml(Config c);
    JSONObject asJSON(Config c);
}
