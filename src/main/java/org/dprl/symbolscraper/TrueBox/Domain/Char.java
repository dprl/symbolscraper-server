package org.dprl.symbolscraper.TrueBox.Domain;

import org.apache.commons.lang3.StringEscapeUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.Boundable;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;
import org.eclipse.jetty.util.ajax.JSON;
import org.json.JSONArray;
import org.json.JSONObject;


import java.util.ArrayList;

import static org.dprl.symbolscraper.TrueBox.SymbolScraperJSONWriter.validateBBOX;

public class Char extends PathWrapper {

    public String value;
    public int charId;

    private Char(ArrayList<CharGlyph> cGlyphs, String value, int charId) {
        this.value = value;
        this.charConstituents = cGlyphs;
        this.graphicConstituents = new ArrayList<>();
        this.charId = charId;
    }

    public Char(CharGlyph initCharGlyph, String value, int charId) {
        charConstituents = new ArrayList<>();
        charConstituents.add(initCharGlyph);
        graphicConstituents = new ArrayList<>();

        updateBoundingBox(initCharGlyph);
        this.value = value;
        this.charId = charId;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            Char c = new Char(charConstituents, value, charId);
            c.boundingBox = boundingBox;
            c.graphicConstituents = graphicConstituents;
            return c;
        }
    }
    public static boolean isNotInvisible(Character _char) {
        switch (Character.getType(_char)) {
            case Character.CONTROL: // \p{Cc}
            case Character.FORMAT: // \p{Cf}
            case Character.PRIVATE_USE: // \p{Co}
            case Character.SURROGATE: // \p{Cs}
            case Character.UNASSIGNED: // \p{Cn}
            case Character.OTHER_SYMBOL: // \OTHER_SYMBOL*/
            case Character.SPACE_SEPARATOR: // \SPACE_SEPARATOR
                return true;
            default:
                return false;
        }
    }
    public static String getType(Character _char){
        switch (Character.getType(_char)) {
            case Character.CONTROL: // \p{Cc}
                return "CONTROL";
            case Character.FORMAT: // \p{Cf}
                return "FORMAT";
            case Character.PRIVATE_USE: // \p{Co}
                return "PRIVATE_USE";
            case Character.SURROGATE: // \p{Cs}
                return "SURROGATE";
            case Character.UNASSIGNED: // \p{Cn}
                return "UNASSIGNED";
            case Character.OTHER_SYMBOL: // \OTHER_SYMBOL
                return "OTHER_SYMBOL";
            case Character.SPACE_SEPARATOR: // \SPACE_SEPARATOR
                return "SPACE_SEPARATOR";
            default:
                return "[other]";
        }
    }

    public JSONObject asJSON(Config c){
        JSONObject root = new JSONObject();
        if (charConstituents.size() > 0 && graphicConstituents.size() > 0) {
            root.put("value", value);
            JSONArray constituents = new JSONArray();
            for (CharGlyph cg : charConstituents) {
                constituents.put(cg.asXml(c));
            }
            for (GraphicGlyph gi : graphicConstituents) {
                constituents.put(gi.asXml(c));
            }
            root.put("constituents",constituents);
        } else {
            if (charConstituents.size() == 1) {
                CharGlyph singleGlyph = charConstituents.get(0);

                if (c.fields.writeFields.charAttributes.writeColor)
                    root.put("RGB", singleGlyph.colorString());

                if(c.fields.writeFields.charAttributes.writeFontName)
                    root.put("fontName", singleGlyph.fontName);

                if(c.fields.writeFields.charAttributes.writeFontSize)
                    root.put("fontSize", singleGlyph.fontSize);

                if(c.fields.writeFields.charAttributes.writeBold)
                    root.put("fontWeight", singleGlyph.fontWeight);

                if(c.fields.writeFields.charAttributes.writeItalicAngle)
                    root.put("italicAngle", singleGlyph.italicAngle);

                root.put("value", singleGlyph.value);

            } else {
                root.put("value", value);
                JSONArray constituents = new JSONArray();
                for (CharGlyph cg : charConstituents) {
                    constituents.put(cg.asXml(c));
                }
                root.put("constituents",constituents);
            }
            if (graphicConstituents.size() == 1) {
                GraphicGlyph singleGraphicsItem = graphicConstituents.get(0);
                root.put("value", singleGraphicsItem.descriptor);
            } else if (graphicConstituents.size() > 1) {
                root.put("value", value);
                JSONArray constituents = new JSONArray();
                for (GraphicGlyph gi : graphicConstituents) {
                    constituents.put(gi.asJSON(c));
                }
                root.put("constituents",constituents);
            }
        }
        if (c.fields.writeFields.writeId)
            root.put("charID", this.charId + "");
        if (c.fields.writeFields.writeCharBBOX) {
            root.put("BBOX", validateBBOX(this.boundingBox));
        }
        Character char_from_value = value.charAt(0);
        if(this.boundingBox.startX == Float.POSITIVE_INFINITY){
            root.put("notVisible", isNotInvisible(char_from_value)?"true":"false");
            root.put("invisible_type",getType(char_from_value));
            String escapedUnicode = StringEscapeUtils.escapeJava(value);
            root.put("unicode", escapedUnicode);
        }
        return root;
    }

    @Override
    public Element asXml(Config c) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("Char");

        if (charConstituents.size() > 0 && graphicConstituents.size() > 0) {
            root.addAttribute("value", value);
            for (CharGlyph cg : charConstituents) {
                root.add(cg.asXml(c));
            }
            for (GraphicGlyph gi : graphicConstituents) {
                root.add(gi.asXml(c));
            }
        } else {
            if (charConstituents.size() == 1) {
                CharGlyph singleGlyph = charConstituents.get(0);

                if (c.fields.writeFields.charAttributes.writeColor)
                    root.addAttribute("RGB", singleGlyph.colorString());

                if(c.fields.writeFields.charAttributes.writeFontName)
                    root.addAttribute("fontName", singleGlyph.fontName);

                if(c.fields.writeFields.charAttributes.writeFontSize)
                    root.addAttribute("fontSize", singleGlyph.fontSize+"");

                if(c.fields.writeFields.charAttributes.writeBold)
                    root.addAttribute("fontWeight", singleGlyph.fontWeight+"");

                if(c.fields.writeFields.charAttributes.writeItalicAngle)
                    root.addAttribute("italicAngle", singleGlyph.italicAngle+"");

                root.addText(singleGlyph.value);

            } else {
                root.addAttribute("value", value);
                for (CharGlyph cg : charConstituents) {
                    root.add(cg.asXml(c));
                }
            }
            if (graphicConstituents.size() == 1) {
                GraphicGlyph singleGraphicsItem = graphicConstituents.get(0);
                root.addText(singleGraphicsItem.descriptor);
            } else if (graphicConstituents.size() > 1) {

                root.addAttribute("value", value);
                for (GraphicGlyph gi : graphicConstituents) {
                    root.add(gi.asXml(c));
                }
            }
        }
        if (c.fields.writeFields.writeId) root.addAttribute("id", this.charId + "");
        if (c.fields.writeFields.writeCharBBOX)
                root.addAttribute("BBOX", this.boundingBox.startX + " " +
                                        +this.boundingBox.startY + " " +
                                        +this.boundingBox.width + " " +
                                        +this.boundingBox.height);

        Character char_from_value = value.charAt(0);
        if(this.boundingBox.startX==Float.POSITIVE_INFINITY){
            root.addAttribute("notVisible", isNotInvisible(char_from_value)?"true":"false");
            root.addAttribute("invisible_type",getType(char_from_value));
            String escapedUnicode = StringEscapeUtils.escapeJava(value);
            root.addAttribute("unicode", escapedUnicode);
        }
        return root;
    }

    @Override
    public void consume(Boundable boundable, boolean update) {

        if (boundable instanceof Char) {
            Char c = (Char) boundable;
            this.value = this.value + c.value;
            charConstituents.addAll(c.charConstituents);
            graphicConstituents.addAll(c.graphicConstituents);
        } else if (boundable instanceof CharGlyph) {
            CharGlyph cg = (CharGlyph) boundable;
            this.value = this.value + cg.value;
            charConstituents.add(cg);
        } else if (boundable instanceof GraphicGlyph) {
            GraphicGlyph gg = (GraphicGlyph) boundable;
            this.value = this.value + gg.descriptor;
            graphicConstituents.add(gg);
        } else if (boundable instanceof Graphic) {
            Graphic g = (Graphic) boundable;
            this.value = this.value + g.descriptor;
            charConstituents.addAll(g.getCharConstituents());
            graphicConstituents.addAll(g.getGraphicConstituents());
        }
        if (update) update();
    }
}
