package org.dprl.symbolscraper.TrueBox.Domain.JTSgraphics;

import org.dprl.symbolscraper.TrueBox.Domain.BBOX;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.GeometryFactory;

import java.util.Dictionary;
import java.util.Hashtable;

public class LabeledGeometryCollection extends GeometryCollection {
    private Dictionary<String, String> attributes = new Hashtable<>();
    private BBOX boundingBox;
    public LabeledGeometryCollection(Geometry[] geometries, GeometryFactory factory, BBOX _boundingBox) {
        super(geometries, factory);
        this.boundingBox = _boundingBox;
    }

    public BBOX getBoundingBox() {
        return boundingBox;
    }

    public void addAttribute(String name, String value){
        attributes.put(name, value);
    }

    public Dictionary<String, String> getAttributes() {
        return attributes;
    }

}
