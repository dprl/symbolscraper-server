package org.dprl.symbolscraper.TrueBox.Domain;

public class BaseLine implements Cloneable{
    float startX;
    float startY;
    float endX;
    float endY;
    public BaseLine(float x, float y, float x2, float y2){
        this.startX=x;
        this.startY=y;
        this.endX=x2;
        this.endY=y2;
    }

    @Override
    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return new BaseLine(
                    this.startX,
                    this.startY,
                    this.endX,
                    this.endY);
        }
    }
}
