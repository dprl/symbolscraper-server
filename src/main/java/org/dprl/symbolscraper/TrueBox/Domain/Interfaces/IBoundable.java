package org.dprl.symbolscraper.TrueBox.Domain.Interfaces;

import org.dprl.symbolscraper.TrueBox.Domain.BBOX;

public interface IBoundable<A> {

    void updateBoundingBox(A b);

    boolean contain(A b);

    boolean overlap(A b);

    float intersectionOverUnion(A b);

    void consume(A intersectingWordEntry, boolean update);

    void update();

    void setBoundingBox(BBOX boundingBox);

    BBOX getBoundingBox();
}
