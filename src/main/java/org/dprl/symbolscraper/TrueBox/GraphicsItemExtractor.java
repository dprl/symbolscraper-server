package org.dprl.symbolscraper.TrueBox;

import org.apache.pdfbox.contentstream.PDFGraphicsStreamEngine;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.image.PDImage;
import org.apache.pdfbox.pdmodel.graphics.pattern.PDAbstractPattern;
import org.apache.pdfbox.pdmodel.graphics.state.PDGraphicsState;
import org.dprl.symbolscraper.TrueBox.Domain.BBOX;
import org.dprl.symbolscraper.TrueBox.Domain.Graphic;
import org.dprl.symbolscraper.TrueBox.Domain.GraphicGlyph;
import org.dprl.symbolscraper.TrueBox.Domain.JTSgraphics.LabeledGeometryCollection;
import org.dprl.symbolscraper.TrueBox.Domain.JTSgraphics.LabeledLineString;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.*;
import java.lang.Math;


class GraphicsItemExtractor extends PDFGraphicsStreamEngine {

    private final GeneralPath linePath = new GeneralPath();
    private int clipWindingRule = -1;
    PDPage page;
    private int graphicId = 0;
    private int graphicGlyphId = 0;
    private final ArrayList<Graphic> allGraphics = new ArrayList<>();

    private Float xMin = null;
    private Float yMin = null;
    private Float xMax = null;
    private Float yMax = null;
    private String descriptor = "";

    private ArrayList<Point2D> points = new ArrayList<>();



    private final ArrayList<LabeledGeometryCollection> geometryObjects = new ArrayList<>();
    private final ArrayList<Geometry> currentGeometryCollection = new ArrayList<>();
    private final GeometryFactory geometryFactory = new GeometryFactory();

    protected GraphicsItemExtractor(PDPage page) {
        super(page);

        this.page=page;
    }

    public ArrayList<Graphic> extractGraphicsItems() throws IOException {
        processPage(page);
        return allGraphics;
    }

    public ArrayList<LabeledGeometryCollection> getGeometryObjects() {
        return geometryObjects;
    }

    @Override
    public void appendRectangle(Point2D p0, Point2D p1, Point2D p2, Point2D p3) throws IOException
    {
        /*
        PDF Command:
        <x> <y> <width> <height> re
        */
        float lineWidth = this.getGraphicsState().getLineWidth();
        ArrayList<Point2D> points = new ArrayList<>(Arrays.asList(p0,p1,p2,p3));
        LabeledLineString lineString = LabeledLineString.createFromPoints(points,geometryFactory);
        lineString.addAttribute("lineWidth",lineWidth+"");
        lineString.addAttribute("typeFromPDF","rectangle");
        currentGeometryCollection.add(lineString);

        descriptor = descriptor + "-rectangle-";
        updateExtrema((float) p0.getX(), (float) p0.getY());
        updateExtrema((float) p1.getX(), (float) p1.getY());
        updateExtrema((float) p2.getX(), (float) p2.getY());
        updateExtrema((float) p3.getX(), (float) p3.getY());

        linePath.moveTo((float) p0.getX(), (float) p0.getY());
        linePath.lineTo((float) p1.getX(), (float) p1.getY());
        linePath.lineTo((float) p2.getX(), (float) p2.getY());
        linePath.lineTo((float) p3.getX(), (float) p3.getY());

        linePath.closePath();
    }

    @Override
    public void drawImage(PDImage pdi) throws IOException
    {
        /*
        PDF command:
        <name> Do
        */
        Random rand = new Random();
        int rand_int1 = rand.nextInt(1000);
        try {
            BufferedImage bi = pdi.getImage();
            File outputfile = new File("images_test/save"+rand_int1+".png");
            ImageIO.write(bi, "png", outputfile);
        }catch (Exception e){
            System.out.println("pinga de gallina: "+e);
        }
        int a = 2;
    }

    @Override
    public void clip(int windingRule) throws IOException
    {
        /*
        PDF Command:
        W / W*
        */
        clipWindingRule = windingRule;
    }

    private void updateExtrema(float x, float y) {

        float lw = getGraphicsState().getLineWidth();
        float lwh = lw/2;

        points.add(new Point2D.Float(x,y));

        if (xMax == null) xMax = x + lwh;
        if (xMin == null) xMin = x - lwh;
        if (yMin == null) yMin = y - lwh;
        if (yMax == null) yMax = y + lwh;

        if (x > xMax) xMax = x + lwh;
        if (x < xMin) xMin = x - lwh;
        if (y < yMin) yMin = y - lwh;
        if (y > yMax) yMax = y + lwh;
    }

    private COSBase lookupCOSName(COSDictionary dictionary, COSName target){
        Set<Map.Entry<COSName,COSBase>> pairs = dictionary.entrySet();
        COSBase retVal = null;
        for (Map.Entry<COSName,COSBase> obj : pairs) {
            COSName key = obj.getKey();

            COSBase value = obj.getValue();

            if (target.equals(key)){
                System.out.println("key: "+key+" value: "+value);
                return value;
            }else if(value instanceof COSObject){
                COSBase base = ((COSObject)value).getObject();
                retVal = lookupCOSName((COSDictionary) base, target);
                if (retVal != null){
                    return retVal;
                }
            }else if(value instanceof COSDictionary){
                retVal = lookupCOSName((COSDictionary) value, target);
                if (retVal != null){
                    return retVal;
                }
            }else{
                int xzc = 2;
            }

        }
        return retVal;
    }


    private void captureGraphicsItem(String triggeredFrom, int windingRule) {
        if ((xMin != null) && (yMin != null) && (xMax != null) && (yMax != null)) {

            float startX = xMin;
            float startY = yMin;
            float width = xMax - xMin;
            float height = yMax - yMin;


            LabeledGeometryCollection geometryCollection = new LabeledGeometryCollection(
                    currentGeometryCollection.toArray(new Geometry[currentGeometryCollection.size()]),
                    geometryFactory,
                    new BBOX(startX, startY, width, height)
            );
            switch (triggeredFrom){
                case "fillPath":
                case "fillAndStroke":
                    geometryCollection.addAttribute("filled",windingRule==0?"even-odd":"non-zero");

                    PDGraphicsState gState = this.getGraphicsState();
                    String strokingColor = "";
                    String fillingColor = "";
                    try{
                        strokingColor = String.format("#%06X", (0xFFFFFF & gState.getStrokingColor().toRGB()));
                        geometryCollection.addAttribute("strokingColor",strokingColor);
                    }catch (Exception e){
                        geometryCollection.addAttribute("strokingColor","<NOT_FOUND>");
                    }
                    try{
                        fillingColor = String.format("#%06X", (0xFFFFFF & gState.getNonStrokingColor().toRGB()));
                        geometryCollection.addAttribute("fillingColor",fillingColor);
                    }catch (Exception e){
                        geometryCollection.addAttribute("fillingColor","<NOT_FOUND>");
                    }
                    break;
            }
            geometryObjects.add(geometryCollection);

            currentGeometryCollection.clear();

            GraphicGlyph b = new GraphicGlyph(graphicGlyphId,
                    new BBOX(startX, startY, width, height),
                    (GeneralPath)linePath.clone(),
                    descriptor,
                    points);

            Graphic newGraphic = new Graphic(b, descriptor, graphicId);


            allGraphics.add(newGraphic);


            graphicId++;
            graphicGlyphId++;

            resetExtrema();
        }
        points = new ArrayList<>();
    }

    private void resetExtrema() {
        xMin = null;
        yMin = null;
        xMax = null;
        yMax = null;
        descriptor = "";
    }

    @Override
    public void moveTo(float x, float y) throws IOException
    {
        /*
        PDF command:
        <x1> <y1> m
        */
        linePath.moveTo(x, y);
        updateExtrema(x, y);
    }

    @Override
    public void lineTo(float x, float y) throws IOException
    {
        /*
        PDF command:
        <x1> <y1> l
        */
        float lineWidth = this.getGraphicsState().getLineWidth();
        ArrayList<Point2D> points = new ArrayList<>(Arrays.asList(this.getCurrentPoint(), new Point2D.Float(x,y)));
        LabeledLineString lineString = LabeledLineString.createFromPoints(points,geometryFactory);

        lineString.addAttribute("lineWidth",lineWidth+"");
        lineString.addAttribute("typeFromPDF","line");

        currentGeometryCollection.add(lineString);

        descriptor = descriptor + GraphicGlyph.graphicsLine;
        updateExtrema(x, y);
        linePath.lineTo(x, y);
    }

    private LabeledLineString createBezierCurve(float x1, float y1, float x2, float y2, float x3, float y3) {
        Shape curve = new CubicCurve2D.Double(
                this.getCurrentPoint().getX(), this.getCurrentPoint().getY(),
                x1, y1,
                x2, y2,
                x3, y3);

        PathIterator iterator = curve.getPathIterator(null, 0.25);

        double[] iteratorBuffer = new double[6];

        ArrayList<Coordinate> coords = new ArrayList<>();
        while (!iterator.isDone()) {
            iterator.currentSegment(iteratorBuffer);
            coords.add(new Coordinate(iteratorBuffer[0], iteratorBuffer[1]));
            iterator.next();
        }
        Coordinate[] list_coords = new Coordinate[coords.size()];
        list_coords = coords.toArray(list_coords);
        return new LabeledLineString(new CoordinateArraySequence(list_coords), this.geometryFactory);
    }

    public String curvedTo(Coordinate[] points){
        int numberPoints = points.length;
        int middlePoint = numberPoints/2;
        Coordinate first = points[0];
        Coordinate middle = points[middlePoint];
        Coordinate last = points[numberPoints-1];
        boolean opensVertical = Math.abs(first.x-last.x) > Math.abs(first.y-last.y);
        if (opensVertical && middle.y > first.y){
            return "bottom";
        }else if(opensVertical && middle.y <= first.y){
            return "top";
        }else if(middle.x <= first.x){
            return "left";
        }else{
            return "right";
        }
    }
    
    @Override
    public void curveTo(float x1, float y1, float x2, float y2, float x3, float y3) throws IOException
    {
        /*
        PDF command:
        <x1> <y1> <x2> <y2> <x3> <y3> c /
        <x2> <y2> <x3> <y3> v /
        <x1> <y1> <x3> <y3> y
        */
        float lineWidth = this.getGraphicsState().getLineWidth();

        LabeledLineString lineString = createBezierCurve(x1, y1, x2, y2, x3, y3);
        
        lineString.addAttribute("curvedTo",curvedTo(lineString.getCoordinates()));
        lineString.addAttribute("lineWidth",lineWidth+"");
        lineString.addAttribute("typeFromPDF","curve");
        lineString.addAttribute("x1",x1+"");
        lineString.addAttribute("x2",x2+"");
        lineString.addAttribute("x3",x3+"");
        lineString.addAttribute("y1",y1+"");
        lineString.addAttribute("y2",y2+"");
        lineString.addAttribute("y3",y3+"");
        currentGeometryCollection.add(lineString);

        Point2D curPoint = this.getCurrentPoint();
        descriptor = descriptor + "-curve-";
        ArrayList<Float> bezierExtrema = getBezierExtrema(
                (float)curPoint.getX(), (float)curPoint.getY(),
                x1, y1,
                x2, y2,
                x3, y3);

        updateExtrema(bezierExtrema.get(0), bezierExtrema.get(1));
        updateExtrema(bezierExtrema.get(0), bezierExtrema.get(3));
        updateExtrema(bezierExtrema.get(2), bezierExtrema.get(1));
        updateExtrema(bezierExtrema.get(2), bezierExtrema.get(1));
        linePath.curveTo(x1, y1, x2, y2, x3, y3);
    }

    @Override
    public Point2D getCurrentPoint()
    {
        return linePath.getCurrentPoint();
    }

    @Override
    public void closePath() {
        /*
        PDF Command:
        h
        */
        if(currentGeometryCollection.size() > 0){
            Coordinate[] coordinates = currentGeometryCollection.get(0).getCoordinates();

            ArrayList<Point2D> points = new ArrayList<>(Arrays.asList(this.getCurrentPoint(),
                    new Point2D.Float((float) coordinates[0].x,(float) coordinates[0].y)));
            LabeledLineString lineString = LabeledLineString.createFromPoints(points,geometryFactory);
            lineString.addAttribute("typeFromPDF","line");
            lineString.addAttribute("lineWidth",
                    ((LabeledLineString) currentGeometryCollection.get(0)).getAttributes().get("lineWidth"));
            currentGeometryCollection.add(lineString);
        }

        //captureGraphicsItem("closePath", -1);
        linePath.closePath();
    }

    @Override
    public void endPath() {
        /*
        PDF command:
        n
        */
        if (clipWindingRule != -1)
        {
            linePath.setWindingRule(clipWindingRule);

            clipWindingRule = -1;
        }

        captureGraphicsItem("endPath", -1);
        linePath.reset();
    }

    @Override
    public void strokePath() {
        /*
        PDF command:
        S / s
        */
        captureGraphicsItem("strokePath",-1);
        linePath.reset();
    }

    @Override
    public void fillPath(int windingRule) {
        /*
        PDF command:
        f / F / f* / F*
        */
        captureGraphicsItem("fillPath",windingRule);
        linePath.reset();
    }

    @Override
    public void fillAndStrokePath(int windingRule) {
        /*
        PDF command:
        b / B / b* / B*
        */
        captureGraphicsItem("fillAndStroke", windingRule);
        linePath.reset();
    }

    @Override
    public void shadingFill(COSName cosn) {
        captureGraphicsItem("shadingFill",-1);
    }

    private ArrayList<Float> getBezierExtrema(float initX, float initY, float x1, float y1, float x2, float y2, float x3, float y3) {

        // Logic for the following computation derived from:
        // https://stackoverflow.com/questions/2587751/an-algorithm-to-find-bounding-box-of-closed-bezier-curves

        ArrayList<Float> tValues = new ArrayList<Float>();
        ArrayList<ArrayList<Float>> bounds = new ArrayList<>();
        bounds.add(0, new ArrayList<>());
        bounds.add(1, new ArrayList<>());

        float a, b, c, t, t1, t2, b2ac, sqrtb2ac;
        for (var i = 0; i < 2; ++i) {
            if (i == 0) {
                b = 6 * initX - 12 * x1 + 6 * x2;
                a = -3 * initX + 9 * x1 - 9 * x2 + 3 * x3;
                c = 3 * x1 - 3 * initX;
            } else {
                b = 6 * initY - 12 * y1 + 6 * y2;
                a = -3 * initY + 9 * y1 - 9 * y2 + 3 * y3;
                c = 3 * y1 - 3 * initY;
            }

            if (Math.abs(a) < 1e-12) // Numerical robustness
            {
                if (Math.abs(b) < 1e-12) // Numerical robustness
                {
                    continue;
                }
                t = -c / b;
                if (0 < t && t < 1)
                {
                    tValues.add(t);
                }
                continue;
            }
            b2ac = b * b - 4 * c * a;
            sqrtb2ac = (float) Math.sqrt(b2ac);
            if (b2ac < 0) {
                continue;
            }
            t1 = (-b + sqrtb2ac) / (2 * a);
            if (0 < t1 && t1 < 1)
            {
                tValues.add(t1);
            }
            t2 = (-b - sqrtb2ac) / (2 * a);
            if (0 < t2 && t2 < 1)
            {
                tValues.add(t2);
            }
        }

        float x, y, j = tValues.size(),
            jlen = j,
                mt;


        while (j-- > 0)
        {
            t = tValues.get((int)j);
            mt = 1 - t;
            x = (mt * mt * mt * initX) + (3 * mt * mt * t * x1) + (3 * mt * t * t * x2) + (t * t * t * x3);
            ArrayList<Float> xBounds = bounds.get(0);
            xBounds.add(x);

            y = (mt * mt * mt * initY) + (3 * mt * mt * t * y1) + (3 * mt * t * t * y2) + (t * t * t * y3);
            bounds.get(1).add(y);

        }

        bounds.get(0).add((int)jlen, initX);
        bounds.get(1).add((int)jlen, initY);
        bounds.get(0).add((int)jlen + 1, x3);
        bounds.get(1).add((int)jlen + 1, y3);


        ArrayList<Float> extrema = new ArrayList<>();
        extrema.add(0, Collections.min(bounds.get(0))); // left
        extrema.add(1, Collections.min(bounds.get(1))); // top
        extrema.add(2,Collections.max(bounds.get(0))); // right
        extrema.add(3,Collections.max(bounds.get(1))); // bottom

        return extrema;
    }
}

