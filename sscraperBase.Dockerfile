FROM openjdk:18-buster

USER root
RUN apt-get update
RUN apt-get install fontconfig -y

# this file exists in /home/devops/fontzip/ of niagara
COPY ./niagarafonts.tar.gz ./
#RUN apt-get update && apt-get install unzip
RUN mkdir -p /usr/share/fonts/
RUN tar -xzvf niagarafonts.tar.gz -C /usr/share/fonts/

RUN wget https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip
RUN unzip apache-maven-3.8.4-bin.zip

ENV PATH /apache-maven-3.8.4/bin:$PATH

CMD ["echo", "helloll from sscraper base"]
