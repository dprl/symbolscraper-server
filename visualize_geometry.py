import tkinter as tk
import random
import json
import sys

pressed = False

class Example(tk.Frame):
    def __init__(self, root):
        tk.Frame.__init__(self, root)
        self.canvas = tk.Canvas(self, width=800, height=800)
        self.xsb = tk.Scrollbar(self, orient="horizontal", command=self.canvas.xview)
        self.ysb = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.ysb.set, xscrollcommand=self.xsb.set)
        self.canvas.configure(scrollregion=(0,0,2000,2000))

        self.xsb.grid(row=1, column=0, sticky="ew")
        self.ysb.grid(row=0, column=1, sticky="ns")
        self.canvas.grid(row=0, column=0, sticky="nsew")
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        #self.fontSize = 10
        

        #Plot some rectangles
        with open(sys.argv[1], 'r') as f:
            data = json.load(f)

            pages = data["pages"]
            for page in pages:                
                
                height = page["BBOX"]["height"]
                width = page["BBOX"]["width"]

                if page["pageID"] != 0:
                    continue

                            #self.canvas.create_rectangle(_char["BBOX"]["x"],height-_char["BBOX"]["y"], 
                            #                             _char["BBOX"]["width"]+_char["BBOX"]["x"],height-_char["BBOX"]["height"]-_char["BBOX"]["y"], outline='blue')
                                                    
                                                    

                
                

                # create canvas                
                
                for _item in page["graphics"]:
                    ids = []

                    #ID
                    self.canvas.create_text(_item['BBOX']["x"], height-_item['BBOX']["y"], text=_item["graphicCollectionID"], fill="red", font=('Helvetica 8 bold'))
                    #BBOX
                    self.canvas.create_rectangle(_item["BBOX"]["x"],height-_item["BBOX"]["y"], 
                                                 _item["BBOX"]["width"]+_item["BBOX"]["x"],height-_item["BBOX"]["height"]-_item["BBOX"]["y"], outline='green')
                    if 'filled' in _item:                        
                        if "approxLine" in _item:
                            self.canvas.create_line(_item["approxLine"]["points"][0]["x"], height-_item["approxLine"]["points"][0]["y"], _item["approxLine"]["points"][1]["x"], height-_item["approxLine"]["points"][1]["y"])
                            continue
                        points_polygon = []
                        for item in _item["GeometryCollection"]:
                            line_width = item["lineWidth"]                            
                            if item["typeFromPDF"] == "line":                                       
                                points_polygon = points_polygon + [item["points"][0]["x"], height-item["points"][0]["y"], item["points"][1]["x"], height-item["points"][1]["y"]]
                            elif item["typeFromPDF"] == "curve":
                                flag = True
                                for point in item["points"]:
                                    if flag:
                                        current = (point["x"],point["y"])
                                        flag = False
                                    else:                                        
                                        points_polygon = points_polygon + [current[0], height-current[1], point["x"], height-point["y"]]
                                        current = (point["x"],point["y"])
                                        # and _item['BBOX']["width"]>item['points'][1]['x']-item['points'][0]['x']
                            elif item["typeFromPDF"] == "rectangle" and _item['BBOX']["width"]*0.7>item['points'][1]['x']-item['points'][0]['x']:
                                self.canvas.create_rectangle(item["points"][0]["x"], height-item["points"][0]["y"], item["points"][1]["x"], height-item["points"][1]["y"], width=item["lineWidth"],
                                                              fill=_item["fillingColor"])
                        if len(points_polygon)==4:
                            self.canvas.create_line(points_polygon[0], points_polygon[1], points_polygon[2], points_polygon[3])
                        if len(points_polygon)>1:                                                        
                            if "fillingColor" in _item:                                
                                filling = _item["fillingColor"]
                            else:
                                filling = ""
                            self.canvas.create_polygon(points_polygon, width=line_width, fill=filling)                                                    
                    else:
                        for item in _item["GeometryCollection"]:                                                
                            if item["typeFromPDF"] == "line":       
                                self.canvas.create_line(item["points"][0]["x"], height-item["points"][0]["y"], item["points"][1]["x"], height-item["points"][1]["y"], width=item["lineWidth"])
                                #self.canvas.create_line(item["points"][0]["x"], height-item["points"][0]["y"], item["points"][1]["x"], height-item["points"][1]["y"], width=0.1,fill="yellow")
                            elif item["typeFromPDF"] == "curve":
                                flag = True
                                for point in item["points"]:
                                    if flag:
                                        current = (point["x"],point["y"])
                                        flag = False
                                    else:
                                        self.canvas.create_line(current[0], height-current[1], point["x"], height-point["y"], width=item["lineWidth"])
                                        current = (point["x"],point["y"])
                            elif item["typeFromPDF"] == "rectangle":
                                self.canvas.create_rectangle(item["points"][0]["x"], height-item["points"][0]["y"], item["points"][1]["x"], height-item["points"][1]["y"], width=item["lineWidth"]) 
                
                for line in page["text"]["textLines"]:
                    for word in line["words"]:
                        for _char in word["constituents"]:
                            if "fontSize" in _char:
                                fontSize = _char["fontSize"]
                            else:
                                fontSize = 1
                            self.canvas.create_text(_char["BBOX"]["x"]+_char["BBOX"]["width"]/2, height-_char["BBOX"]["y"]-_char["BBOX"]["height"]/2, text=_char["value"], 
                                                    fill="black",  font=("Helvetica",fontSize+10))
                
        # This is what enables using the mouse:
        self.canvas.bind("<ButtonPress-1>", self.move_start)
        self.canvas.bind("<B1-Motion>", self.move_move)

        self.canvas.bind("<ButtonPress-2>", self.pressed2)
        self.canvas.bind("<Motion>", self.move_move2)

        #linux scroll
        self.canvas.bind("<Button-4>", self.zoomerP)
        self.canvas.bind("<Button-5>", self.zoomerM)
        #windows scroll
        self.canvas.bind("<MouseWheel>",self.zoomer)
        # Hack to make zoom work on Windows
        root.bind_all("<MouseWheel>",self.zoomer)

    #move
    def move_start(self, event):
        self.canvas.scan_mark(event.x, event.y)
    def move_move(self, event):
        self.canvas.scan_dragto(event.x, event.y, gain=1)

    #move
    def pressed2(self, event):
        global pressed
        pressed = not pressed
        self.canvas.scan_mark(event.x, event.y)
    def move_move2(self, event):
        if pressed:
            self.canvas.scan_dragto(event.x, event.y, gain=1)

    #windows zoom
    def zoomer(self,event):
        if (event.delta > 0):
            self.canvas.scale("all", event.x, event.y, 1.1, 1.1)
            self.fontSize = self.fontSize * 1.1
        elif (event.delta < 0):
            self.canvas.scale("all", event.x, event.y, 0.9, 0.9)
            self.fontSize = self.fontSize * 0.9
        self.canvas.configure(scrollregion = self.canvas.bbox("all"))
        for child_widget in self.canvas.find_withtag("text"):
            self.canvas.itemconfigure(child_widget, font=("Helvetica", int(self.fontSize)))
        print(self.fontSize)

    #linux zoom
    def zoomerP(self,event):
        self.canvas.scale("all", event.x, event.y, 1.1, 1.1)
        self.canvas.configure(scrollregion = self.canvas.bbox("all"))
    def zoomerM(self,event):
        self.canvas.scale("all", event.x, event.y, 0.9, 0.9)
        self.canvas.configure(scrollregion = self.canvas.bbox("all"))

if __name__ == "__main__":
    root = tk.Tk()
    Example(root).pack(fill="both", expand=True)
    root.mainloop()
'''
for line in page["lines"]:
                    for word in page["lines"][line]["words"]:
                        word_data = page["lines"][line]["words"][word]
                        for char in word_data["chars"]:
                            _char = word_data["chars"][char]
                            self.canvas.create_text(_char["BBOX"]["x"], _char["BBOX"]["y"], text=_char["value"], fill="black",  font=('Helvetica',_char["fontSize"]))
'''
