build:
	mvn clean package

start-server:
	java -jar ./target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar

start-docker:
	docker run -p 7002:7002 dprl/symbolscraper-server:latest

example-1:
	java -jar target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar -f words.pdf -b bounded.pdf

larger-example:
	java -jar target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar -f largerExample.pdf -o largerSymbols.xml -b largerBBs.pdf --processors SpliceGraphicsItemsAndCharsWithIOU IdentifyMathFromSplicedStructure

graphic-descriptor-example:
	java -jar target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar \
    -f debugging_pdfs/from_svgs/mol4_latex.pdf -o largerSymbols.xml \
    -j out.json -b largerBBs.pdf --writePath true --writePageBorderBBOX true \
    --writePoints true --drawItemID true --writeFontSize true \
    --processors ExplainGeometry
